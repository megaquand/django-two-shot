from django.shortcuts import render
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required


@login_required
def show_Receipts(request):
    Receipt_List = Receipt.objects.filter(purchaser=request.user)
    context = {
        "Receipt_List": Receipt_List
    }
    return render(request, "list.html", context)
