from django.urls import path
from receipts.views import show_Receipts


urlpatterns = [
    path("", show_Receipts, name="home"),
]
